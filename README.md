# Fast DDS ShapesDemo for Android
Fast DDS Shapes for Android is application demonstrating communication
Fast DDS implementation of RTPS protocol. In this demo shapes (Circle, Square and Triangle) of various colors and shapes are moving on canvas. Those shapes are either generated locally (and shared via DDS) or they can be generated remotely and are being shared to your respectable device. This demo can be used as demonstration of Fast DDS capability and demonstration of the fact, that it is possible to use Fast DDS on Android.

## Installation guide
To run this application, Android Studio, Fast DDS library compiled for aarch64 and [Android NDK][ndk-download] is needed.

### Fast DDS Compilation guide (for Linux)
This guide is modified version of [official Fast DDS installation guide][dds-linux].  
To install Fast DDS you need following packages:
- cmake, g++, pip3, wget and Git

When using Debian, or Debian based Linux distros (i.e. Ubuntu) you can use:  
`sudo apt install cmake g++ python3-pip wget git`  
- Asio library, TinyXML2 library, OpenSSL, Libp11, SoftHSM

When using Debian, or Debian based Linux distros (i.e. Ubuntu) you can use:  
`sudo apt install libasio-dev libtinyxml2-dev libssl-dev libp11-dev libengine-pkcs11-openssl softhsm2`

Note that the softhsm2 package creates a new group called softhsm. In order to grant access to the HSM module a user must belong to this group. This can be done running:  
`sudo usermod -a -G softhsm <user>`

OpenSSL access HSM and other hardware devices through its engine functionality. In order to set up a new engine the OpenSSL configuration files (usually /etc/ssl/openssl.cnf) must be updated specifying the libp11 and hardware module (here SoftHSM) dynamic libraries location.

This configuration step can be avoided using p11kit which allows OpenSSL to find PKCS#11 devices on runtime without static configuration. This kit is often available through the Linux distribution package manager. On Ubuntu, for example:

`sudo apt install libengine-pkcs11-openssl`

Once installed, to check p11kit is able to find the SoftHSM module use:

`p11-kit list-modules`

In order to check if OpenSSL is able to access PKCS#11 engine use:

`openssl engine pkcs11 -t`

### Compilation

- Download and extract [Android NDK][ndk-download]
- Create a directory for this library and its dependencies. Recommended is `~\Fast-DDS` (please note, that this directory is used in this guide)
```sh
mkdir ~/Fast-DDS
```
- Install and cross-compile Foonathan Memory using following commands:
  ```sh
  cd ~/Fast-DDS
  git clone https://github.com/eProsima/foonathan_memory_vendor.git
  mkdir foonathan_memory_vendor/build
  cd foonathan_memory_vendor/build
  cmake .. -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install -DBUILD_SHARED_LIBS=ON -DCMAKE_TOOLCHAIN_FILE=path/to/android-ndk-r23b/build/cmake/android.toolchain.cmake -DANDROID_ABI=arm64-v8a -DANDROID_NATIVE_API_LEVEL=24
  cmake --build . --target install
  ```
- Install and cross-compile Fast-CDR library
  ```sh
  cd ~/Fast-DDS
  git clone https://github.com/eProsima/Fast-CDR.git
  mkdir Fast-CDR/build
  cd Fast-CDR/build
  cmake .. -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install -DCMAKE_TOOLCHAIN_FILE=path/to/android-ndk-r23b/build/cmake/android.toolchain.cmake -DANDROID_ABI=arm64-v8a -DANDROID_NATIVE_API_LEVEL=24
  cmake --build . --target install
  ```
- Clone Fast DDS itself
  ```sh
  cd ~/Fast-DDS
  git clone https://github.com/eProsima/Fast-DDS.git
  mkdir Fast-DDS/build
  cd Fast-DDS/build
  cmake ..  -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install -DCMAKE_TOOLCHAIN_FILE=path/to/android-ndk-r23b/build/cmake/android.toolchain.cmake -DANDROID_ABI=arm64-v8a -DANDROID_NATIVE_API_LEVEL=24 -DCMAKE_BUILD_TYPE=Release -DTHIRDPARTY=FORCE -DCMAKE_FIND_ROOT_PATH=~/Fast-DDS/install
  cmake --build . --target install
  ```

Compiled library can be found at `~/Fast-DDS/install`

## Launching app

Android Studio should now automatically recognize project folder. When opening project (via File -> Open) Android Studio indicates recognition of Android project by using green android icon for that folder (this may take some time). Only step that is neccesary is modification of library path in CMAKE.
- In file CMakeList.txt (app/src/main/cpp) modify `DDS_PATH` (on line 6) to location of the library, that you compiled in the previous step.


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[dds-linux]: <https://fast-dds.docs.eprosima.com/en/latest/installation/sources/sources_linux.html>
[ndk-download]: <https://developer.android.com/ndk/downloads>
[dds-repo]: <https://github.com/eProsima/Fast-DDS>
