// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file ShapeSubscriber.cpp
 *
 */

#include "ShapePubSubTypes.h"

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastdds/dds/subscriber/Subscriber.hpp>
#include <fastdds/dds/subscriber/DataReader.hpp>
#include <fastdds/dds/subscriber/DataReaderListener.hpp>
#include <fastdds/dds/subscriber/qos/DataReaderQos.hpp>
#include <fastdds/dds/subscriber/SampleInfo.hpp>
#include <android/log.h>
#include <jni.h>

using namespace eprosima::fastdds::dds;


jmethodID gMethod;
jobject gObject;
JavaVM *java_vm;

class ShapeSubscriber {

private:
    DomainParticipant* participant_;
    Subscriber* subscriber_;
    DataReader* reader_;
    Topic* topic_;
    TypeSupport type_;


public: class SubListener : public DataReaderListener {

    private: JNIEnv *env;

    public:

        std::string shape;

        SubListener() : samples_(0) {
            //java_vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6);
            //java_vm->AttachCurrentThread(&env, NULL);
            //attach();
        }

        ~SubListener() override {
            //java_vm->DetachCurrentThread();
        }

        //void on_subscription_matched(DataReader*, const SubscriptionMatchedStatus& info) override {}
        void attach() {
            //java_vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6);
            //java_vm->AttachCurrentThread(&env, NULL);
        }

        void on_data_available(DataReader* reader) override {

            __android_log_print(ANDROID_LOG_VERBOSE, "cpptest", "Calling JVM");
            java_vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6);
            java_vm->AttachCurrentThread(&env, NULL);

            SampleInfo info;
            if (reader->take_next_sample(&message_, &info) == ReturnCode_t::RETCODE_OK) {
                if (info.valid_data) {
                    int xa = message_.x();
                    int xb = message_.y();
                    int xs = message_.shapesize();
                    env->CallVoidMethod(gObject, gMethod, xa, xb, xs, env->NewStringUTF(message_.color().c_str()), env->NewStringUTF(shape.c_str()));
                }
            }

            //java_vm->DetachCurrentThread();
        }

        ShapeType message_;
        std::atomic_int samples_;

    } listener_;

public:

    ShapeSubscriber()
        : participant_(nullptr)
        , subscriber_(nullptr)
        , topic_(nullptr)
        , reader_(nullptr)
        , type_(new ShapeTypePubSubType())
    {}

    virtual ~ShapeSubscriber() {
        if (reader_ != nullptr) {
            subscriber_->delete_datareader(reader_);
        } if (topic_ != nullptr) {
            participant_->delete_topic(topic_);
        } if (subscriber_ != nullptr) {
            participant_->delete_subscriber(subscriber_);
        }
        DomainParticipantFactory::get_instance()->delete_participant(participant_);
    }

    //!Initialize the subscriber
    bool init(std::string _shape = "Square") {

        DomainParticipantQos participantQos;
        participantQos.name("ANDROID SUBSCRIBER" + _shape);
        participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);
        type_.register_type(participant_);
        topic_ = participant_->create_topic(_shape, "ShapeType", TOPIC_QOS_DEFAULT);
        subscriber_ = participant_->create_subscriber(SUBSCRIBER_QOS_DEFAULT, nullptr);
        reader_ = subscriber_->create_datareader(topic_, DATAREADER_QOS_DEFAULT, &listener_);
        listener_.shape = _shape;

        return true;
    }

};

extern "C"
JNIEXPORT jlong JNICALL
Java_cz_cvut_fel_marunluk_fastdds_1shapes_HandlerThread_initSubscriber(JNIEnv *env, jobject thiz, jstring shape) {
    ShapeSubscriber* subscriber = new ShapeSubscriber();
    if (subscriber->init(env->GetStringUTFChars(shape, NULL))) {
        subscriber->listener_.attach();
        env->GetJavaVM(&java_vm);
        gObject = env->NewGlobalRef(thiz);
        jclass lClass = env->FindClass("cz/cvut/fel/marunluk/fastdds_shapes/HandlerThread");
        gMethod = env->GetMethodID(lClass, "putShape", "(IIILjava/lang/String;Ljava/lang/String;)V");
        return (jlong) subscriber;
    }
    return 0;
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_cz_cvut_fel_marunluk_fastdds_1shapes_HandlerThread_killSubscriber(JNIEnv *env, jobject thiz, jlong pointer) {
    ShapeSubscriber* subscriber = (ShapeSubscriber *) pointer;
    delete subscriber;
    return true;
}