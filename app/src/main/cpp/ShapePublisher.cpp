// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file ShapePublisher.cpp
 *
 */

#include "ShapePubSubTypes.h"

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastdds/dds/publisher/Publisher.hpp>
#include <fastdds/dds/publisher/DataWriter.hpp>
#include <fastdds/dds/publisher/DataWriterListener.hpp>
#include <fastdds/rtps/transport/UDPv4TransportDescriptor.h>
#include <android/log.h>
#include <jni.h>

using namespace eprosima::fastdds::dds;

class ShapePublisher {
private:

    ShapeType message_;
    DomainParticipant* participant_;
    Publisher* publisher_;
    Topic* topic_;
    DataWriter* writer_;
    TypeSupport type_;


public:

    ShapePublisher()
        : participant_(nullptr)
        , publisher_(nullptr)
        , topic_(nullptr)
        , writer_(nullptr)
        , type_(new ShapeTypePubSubType()) {
    }

    virtual ~ShapePublisher() {

        if (writer_ != nullptr) {
            publisher_->delete_datawriter(writer_);
        } if (publisher_ != nullptr) {
            participant_->delete_publisher(publisher_);
        } if (topic_ != nullptr) {
            participant_->delete_topic(topic_);
        }
        DomainParticipantFactory::get_instance()->delete_participant(participant_);
    }

    //!Initialize the publisher
    bool init(std::string _shape = "Square") {

        message_.color("BLUE");
        message_.x(50);
        message_.y(50);
        message_.shapesize(50);

        DomainParticipantQos participantQos;
        participantQos.name("ANDROID PUBLISHER: " + _shape);
        participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);
        type_.register_type(participant_);
        topic_ = participant_->create_topic(_shape, "ShapeType", TOPIC_QOS_DEFAULT);
        publisher_ = participant_->create_publisher(PUBLISHER_QOS_DEFAULT, nullptr);
        writer_ = publisher_->create_datawriter(topic_, DATAWRITER_QOS_DEFAULT);

        __android_log_print(ANDROID_LOG_VERBOSE, "cpptest", "Inicializuji publishera");

        return true;
    }

    //!Send a publication
    bool publish(int _x = 50, int _y = 50, int _size = 50, std::string _color = "BLUE") {
        if (1 > 0) {
            message_.x(_x);
            message_.y(_y);
            message_.shapesize(_size);
            message_.color(_color);
            writer_->write(&message_);
            __android_log_print(ANDROID_LOG_VERBOSE, "cpptest", "Odesílám publish");
            return true;
        }
        return false;
    }

};


extern "C"
JNIEXPORT jlong JNICALL
Java_cz_cvut_fel_marunluk_fastdds_1shapes_HandlerThread_initPublisher(JNIEnv *env, jobject thiz, jstring shape) {
    ShapePublisher* publisher = new ShapePublisher();
    if (publisher->init(env->GetStringUTFChars(shape, NULL))) {
        return (jlong) publisher;
    }
    return 0;
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_cz_cvut_fel_marunluk_fastdds_1shapes_HandlerThread_killPublisher(JNIEnv *env, jobject thiz, jlong pointer) {
    ShapePublisher* publisher = (ShapePublisher*) pointer;
    delete publisher;
    return true;
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_cz_cvut_fel_marunluk_fastdds_1shapes_HandlerThread_sendPublisher(JNIEnv *env, jobject thiz, jlong pointer, jint coord_x,
                                                        jint coord_y, jint size, jstring color) {
    ShapePublisher* publisher = (ShapePublisher*) pointer;
    if (publisher->publish(coord_x, coord_y, size, env->GetStringUTFChars(color, NULL))) {
        return true;
    }
    return false;
}