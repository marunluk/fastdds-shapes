package cz.cvut.fel.marunluk.fastdds_shapes

import android.graphics.Color
import android.graphics.Point
import android.util.Log
import java.util.concurrent.ConcurrentHashMap

class HandlerThread(val shapeMap: ConcurrentHashMap<DDSShape, Point> ) : Runnable {

    private val startX = 100
    private val startY = 100
    private val screenX = 235
    private val screenY = 265
    private var squareSubscriber: Long = 0
    private var circleSubscriber: Long = 0
    private var triangleSubscriber: Long = 0
    private var squarePublisher: Long = 0
    private var circlePublisher: Long = 0
    private var trianglePublisher: Long = 0
    private var subSquare = false
    private var subCircle = false
    private var subTriangle = false
    private var pubShapes: HashMap<DDSShape, Point> = HashMap<DDSShape, Point>()
    var pubCnt = 0

    override fun run() {
        while (subSquare or subCircle or subTriangle or (pubCnt > 0)) {
            for ((shape_k, dir_v) in pubShapes) {
                shapeMap[shape_k]!!.x = (shapeMap[shape_k]!!.x + dir_v.x) % screenX
                shapeMap[shape_k]!!.y = (shapeMap[shape_k]!!.y + dir_v.y) % screenY
                var publisherPtr = 0L
                when (shape_k.shape) {
                    "Square" -> publisherPtr = squarePublisher
                    "Circle" -> publisherPtr = circlePublisher
                    "Triangle" -> publisherPtr = trianglePublisher
                }
                if (publisherPtr != 0L) {
                    sendPublisher(publisherPtr, shapeMap[shape_k]!!.x, shapeMap[shape_k]!!.y, shape_k.size, DemoTools.colorToString(shape_k.color))
                }
            }
            Thread.sleep(100);
        }
    }

    fun startSubSquare() {
        if ((squareSubscriber == 0L) and !subSquare) {
            squareSubscriber = initSubscriber("Square")
            Log.d("TEST", "Zapinam ctverec $squareSubscriber")
            subSquare = true
        }
    }

    fun startSubCircle() {
        if ((circleSubscriber == 0L) and !subCircle) {
            circleSubscriber = initSubscriber("Circle")
            Log.d("TEST", "Zapinam kolecko $circleSubscriber")
            subCircle = true
        }
    }

    fun startSubTriangle() {
        if ((triangleSubscriber == 0L) and !subTriangle) {
            triangleSubscriber = initSubscriber("Triangle")
            Log.d("TEST", "Zapinam trojuhelnik $triangleSubscriber")
            subTriangle = true
        }
    }

    fun stopSubSquare() {
        if ((squareSubscriber != 0L) and subSquare) {
            Log.d("TEST", "Vypinam ctverec")
            killSubscriber(squareSubscriber)
            squareSubscriber = 0
            subSquare = false
        }
    }

    fun stopSubCircle() {
        if ((circleSubscriber != 0L) and subCircle) {
            Log.d("TEST", "Vypinam kolecko")
            killSubscriber(circleSubscriber)
            circleSubscriber = 0
            subCircle = false
        }
    }

    fun stopSubTriangle() {
        if ((triangleSubscriber != 0L) and subTriangle) {
            Log.d("TEST", "Vypinam trojuhelnik")
            killSubscriber(triangleSubscriber)
            triangleSubscriber = 0
            subTriangle = false
        }
    }

    fun startPubShape(shape: DDSShape, dir: Point) {
        when (shape.shape) {
            "Square" -> {
                if (squarePublisher == 0L) {
                    squarePublisher = initPublisher("Square")
                }
            }
            "Circle" -> {
                if (circlePublisher == 0L) {
                    circlePublisher = initPublisher("Circle")
                }
            }
            "Triangle" -> {
                if (trianglePublisher == 0L) {
                    trianglePublisher = initPublisher("Triangle")
                }
            }
        }
        pubShapes[shape] = dir
        shapeMap[shape] = Point(startX, startY)
    }

    fun putShape(x: Int, y: Int, size: Int, color: String, type: String) {
        shapeMap[DDSShape(DemoTools.stringToColor(color), type, size)] = Point(x, y)
    }

    external fun initPublisher(shape: String): Long

    external fun initSubscriber(shape: String): Long

    external fun killPublisher(pointer: Long): Boolean

    external fun killSubscriber(pointer: Long): Boolean

    external fun sendPublisher(pointer: Long, coordX: Int, coordY: Int, size: Int, color: String): Boolean


    companion object {
        // Used to load the 'fastdds_shapes' library on application startup.
        init {
            System.loadLibrary("fastdds_shapes_native")
        }
    }

}