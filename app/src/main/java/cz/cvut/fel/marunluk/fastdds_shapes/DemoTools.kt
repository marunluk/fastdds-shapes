package cz.cvut.fel.marunluk.fastdds_shapes

import android.graphics.Color

class DemoTools {

    companion object {
        fun colorToString(input: Int): String {
            when (input) {
                Color.RED -> return "RED"
                Color.BLUE -> return "BLUE"
                Color.GREEN -> return "GREEN"
                Color.YELLOW -> return "YELLOW"
                Color.parseColor("#FF9900") -> return "ORANGE"
                Color.CYAN -> return "CYAN"
                Color.MAGENTA -> return "MAGENTA"
                Color.parseColor("#AA00FF") -> return "PURPLE"
                Color.GRAY -> return "GRAY"
                Color.BLACK -> return "BLACK"
            }
            return "RED"
        }

        fun stringToColor(input: String): Int {
            when (input) {
                "ORANGE" -> return Color.parseColor("#FF9900")
                "PURPLE" -> return Color.parseColor("#AA00FF")
            }
            return Color.parseColor(input)
        }
    }
}