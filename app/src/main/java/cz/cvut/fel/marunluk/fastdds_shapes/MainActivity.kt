package cz.cvut.fel.marunluk.fastdds_shapes

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.ArcShape
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.RectShape
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import java.util.concurrent.ConcurrentHashMap


class MainActivity : AppCompatActivity() {

    private val screenX = 235
    private val screenY = 265
    private val shView: ImageView by lazy { findViewById<ImageView>(R.id.shapesView) }
    private val shBitmap = Bitmap.createBitmap(screenX, screenY, Bitmap.Config.ARGB_8888)
    private val shCanvas = Canvas(shBitmap)

    private var shapeMap = ConcurrentHashMap<DDSShape, Point>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        shView.foreground = BitmapDrawable(resources, shBitmap)

        // Create frame around canvas
        val backImageView = findViewById<ImageView>(R.id.backView)
        val backBitmap = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888)
        val backCanvas = Canvas(backBitmap)
        val squareSubSwitch = findViewById<Switch>(R.id.switchSquare)
        val circleSubSwitch = findViewById<Switch>(R.id.switchCircle)
        val triangleSubSwitch = findViewById<Switch>(R.id.switchTriangle)
        val pubButton = findViewById<Button>(R.id.pubButton)
        val bton = findViewById<Button>(R.id.button2)
        val shapeSpin = findViewById<Spinner>(R.id.shapeSpinner)
        val colorSpin = findViewById<Spinner>(R.id.colorSpinner)
        val sizeSelect = findViewById<EditText>(R.id.sizeText)
        val colorAdapter = ArrayAdapter.createFromResource(this, R.array.Colors, android.R.layout.simple_spinner_item)
        val shapeAdapter = ArrayAdapter.createFromResource(this, R.array.Shapes, android.R.layout.simple_spinner_item)


        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        shapeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        colorSpin.adapter = colorAdapter
        shapeSpin.adapter = shapeAdapter
        backImageView.background = BitmapDrawable(resources, backBitmap)
        backCanvas.drawColor(Color.BLACK)
        shCanvas.drawColor(Color.WHITE)

        shapeLoop()

        val subHandler = HandlerThread(shapeMap)
        val subHandlerT = Thread(subHandler)
        subHandler.pubCnt += 1
        subHandlerT.start()

        val pubHandler = HandlerThread(shapeMap)
        val pubHandlerT = Thread(pubHandler)
        pubHandler.pubCnt += 1
        pubHandlerT.start()

        squareSubSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                subHandler.startSubSquare()
            } else {
                subHandler.stopSubSquare()
            }
        }

        circleSubSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                subHandler.startSubCircle()
            } else {
                subHandler.stopSubCircle()
            }
        }

        triangleSubSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                subHandler.startSubTriangle()
            } else {
                subHandler.stopSubTriangle()
            }
        }

        pubButton.setOnClickListener {
            pubHandler.startPubShape(DDSShape(DemoTools.stringToColor(colorSpin.selectedItem.toString()), shapeSpin.selectedItem.toString(), Integer.parseInt(sizeSelect.text.toString())), Point(5,5))
        }

        bton.setOnClickListener {
            Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show()
        }

    }

    private fun shapeLoop() {
        Thread(Runnable {
            while (true) {
                runOnUiThread { shCanvas.drawColor(Color.WHITE) }

                for ((shape_k, coords_v) in shapeMap) {
                    val currentShape = getShape(shape_k, coords_v)
                    runOnUiThread {
                        currentShape.draw(shCanvas)
                    }
                }
                shView.invalidate()
                Thread.sleep(25)
            }
        }).start()
    }

    fun getShape(shape_t: DDSShape, cord: Point) : ShapeDrawable {
        var shapeDrawable = ShapeDrawable()
        shapeDrawable.shape = OvalShape()

        when (shape_t.shape) {
            //"Square" -> shapeDrawable.shape = RectShape()
            "Square" -> shapeDrawable.shape = RectShape()
            "Circle" -> shapeDrawable.shape = OvalShape()
            else     -> shapeDrawable.shape = ArcShape(50.toFloat(),70.toFloat())
            //TODO: Pridat normalni trojuhelnik a default
        }

        shapeDrawable.setBounds(cord.x - (shape_t.size/2), cord.y - (shape_t.size/2), cord.x + (shape_t.size/2), cord.y + (shape_t.size/2))
        shapeDrawable.paint.color = shape_t.color
        return shapeDrawable
    }
    /**
     * A native method that is implemented by the 'fastdds_shapes' native library,
     * which is packaged with this application.
     */



    companion object {
        // Used to load the 'fastdds_shapes' library on application startup.
        init {
            System.loadLibrary("fastdds_shapes_native")
        }
    }
}