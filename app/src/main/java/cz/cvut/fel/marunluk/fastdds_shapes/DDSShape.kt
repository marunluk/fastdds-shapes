package cz.cvut.fel.marunluk.fastdds_shapes

class DDSShape(val color: Int, val shape: String, val size: Int) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DDSShape

        if (color != other.color) return false
        if (shape != other.shape) return false
        if (size != other.size) return false

        return true
    }

    override fun hashCode(): Int {
        var result = color
        result = 31 * result + shape.hashCode()
        result = 31 * result + size
        return result
    }

}